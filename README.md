# Get-azure-RateCard

## Description

This script connects to the Microsoft.Commerce/RateCard API and retrieves the latest RateCard based on the script parameters.
It then writes the result in a .json file.

## Usage

Edit the following values from the `config` dictionary at the top of the file `azure_ratecard.py`


- TENANT_ID
- APPLICATION_ID
- APPLICATION_SECRET
- SUBSCRIPTION_ID

Run the script:

    $> pipenv install
    $> pipenv shell
    $> python ./azure_ratecard.py --offer MS-AZR-0003P --locale us-US --region US --currency USD

The command above will create a file `azr-ratecard-MS-AZR-0003P-US-USD.json` containing the RateCard for the Pay-as-you-go offer in region US using USD as currency.