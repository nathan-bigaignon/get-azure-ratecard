import requests
import argparse
import logging
from azure.common.credentials import ServicePrincipalCredentials

logger = logging.getLogger('azure_ratecard')
logging.basicConfig(level=logging.INFO)

config = {
    'TENANT_ID': '',
    'APPLICATION_ID': '',
    'APPLICATION_SECRET': '',
    'SUBSCRIPTION_ID': '',
}


class AzureRateCard:
    def __init__(self, args):
        self.arguments = args
        self.access_token = None

    def authenticate(self):
        """
        Requests an access token using the Azure SDK. The token is required to communicate with the
        RateCard API.
        :return: None
        """
        credentials = ServicePrincipalCredentials(
            client_id=config['APPLICATION_ID'],
            secret=config['APPLICATION_SECRET'],
            tenant=config['TENANT_ID']
        )
        self.access_token = credentials.token['access_token']

    def get_rate_card(self):
        """
        Retrieve and write a RateCard to a .json file using the provided parameters.
        :return: None
        """
        domain = "management.azure.com:443"
        route = "subscriptions/{}/providers/Microsoft.Commerce/RateCard".format(config['SUBSCRIPTION_ID'])
        option = "api-version=2016-08-31-preview"
        _filter = "OfferDurableId eq '{}' and Currency eq '{}' and  Locale eq '{}' and RegionInfo eq '{}'".format(
            self.arguments.offer,
            self.arguments.currency,
            self.arguments.locale,
            self.arguments.region
        )
        rate_card_url = "https://{domain}/{route}?{option}&$filter={filter}".format(
            domain=domain,
            route=route,
            option=option,
            filter=_filter
        )

        logger.info('{rate_card_url}'.format(rate_card_url=rate_card_url))

        response = requests.get(rate_card_url, allow_redirects=False, headers={'Authorization': 'Bearer {}'.format(
            self.access_token)})

        # Look at response headers to get the redirect URL
        try:
            redirect_url = response.headers['location']
        except KeyError:
            logging.error(response.content)
            exit()

        # Get the ratecard content by making another call to go the redirect URL
        rate_card = requests.get(redirect_url)

        filename = 'azr-ratecard-{}-{}-{}.json'.format(self.arguments.offer,
                                                       self.arguments.region,
                                                       self.arguments.currency)

        with open(filename, "w") as file:
            file.write(str(rate_card.content))

        logging.info('{filename} created successfully'.format(filename=filename))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parser script parameters.')
    parser.add_argument('--offer', dest='offer', default='MS-AZR-0003P')
    parser.add_argument('--locale', dest='locale', default='us-US')
    parser.add_argument('--region', dest='region', default='US')
    parser.add_argument('--currency', dest='currency', default='USD')

    args = parser.parse_args()
    azure_card = AzureRateCard(args)
    azure_card.authenticate()
    azure_card.get_rate_card()
